import axios from "axios";;
export const fetchSummaryaxios = () => {
    return axios.get(`http://a1e8-111-94-81-0.ap.ngrok.io/getsummary`)
        .then(data => {return data})
}

export const fetchLoginaxios = (uname:string,pw:string) => {
    
    return axios.post(`http://a1e8-111-94-81-0.ap.ngrok.io/login`,{
        "username": uname,
        "password": pw
    })
    .then(data => {return data})
}

export const fetchOverviewaxios = () => {
    return axios.get(`http://a1e8-111-94-81-0.ap.ngrok.io/gettablebattery`)
        .then(data => {return data})
}
