import { SummaryAction, SummaryState } from '../reducers/summary/types';
import { LoginAction } from '../reducers/login/types';
import { OverviewAction } from '../reducers/overview/types';

export const fetchSummary = (test:any): SummaryAction => ({

    type: "GET_SUMMARY",
    data: test
});

export const fetchLogin = (test:any): LoginAction => ({

    type: "POST_LOGIN",
    data: test
});

export const fetchOverview = (test:any): OverviewAction => ({

    type: "GET_OVERVIEW",
    data: test
});




