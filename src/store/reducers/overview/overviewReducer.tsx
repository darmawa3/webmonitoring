import { OverviewAction, OverviewModel } from './types';



export const overviewReducer = (
    state: OverviewModel = {
        overview: []
    },
    action: OverviewAction
) => {
    switch (action.type) {
        case "GET_OVERVIEW": {
            return { ...state.overview, overview:  [action.data[0]] };
        }

        default:
            return state;
    }
};

