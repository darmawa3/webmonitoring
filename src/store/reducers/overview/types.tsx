export interface OverviewState {
    v_in: number,
    v_out : number,
    i_in : number,
    i_out : number,
    p_in : number,
    p_out : number
}
const OverviewStateKeysEnum: { [K in keyof Required<OverviewState>]: K } = {
    v_in: 'v_in',
    v_out: 'v_out',
    i_in :'i_in',
    i_out :'i_out',
    p_in : 'p_in',
    p_out : 'p_out'
  };
  
const OverviewStateKeys = Object.values(OverviewStateKeysEnum);


export function isOverview(obj: any): obj is OverviewState {
    let arrofkeys = OverviewStateKeys;
    let num = 0;
    let bool = false;
    for (let i = 0; i <= arrofkeys.length; i++) {
      if (arrofkeys[i] in obj[0]) {
        num += 1;
      }
    }
    if (num == arrofkeys.length) {
      bool = true;
    }
    return bool;
  }


export interface OverviewModel {
    overview: OverviewState[],
}

export type OverviewA = {
    type: "GET_OVERVIEW";
    data: OverviewState[]
};


export type OverviewAction = OverviewA;