import { combineReducers } from 'redux'
import { summaryReducer } from './summary/summaryReducer'
import { loginReducer } from './login/loginReducer'
import { overviewReducer } from './overview/overviewReducer'
// const  combineReducer = summaryReducer | loginReducer
export const reducers = combineReducers({
  summary: summaryReducer,
  login : loginReducer,
  overview : overviewReducer
})

