import { createStore , applyMiddleware, combineReducers } from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
// import { summaryReducer } from "./summary/summaryReducer";
import { reducers } from './index';

const composeEnhancers = composeWithDevTools({});
export const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));