import { SummaryAction, SummaryModel } from './types';



export const summaryReducer = (
  state: SummaryModel ={
    // summaryList: [],
    summary: []
  },
  action: SummaryAction
) => {
  switch (action.type) {
    case "GET_SUMMARY": {
      if (state.summary.length == 0) {
        for (let i = 0; i <= action.data.length; i++) {
          state.summary.push(action.data[i]);
        }
      }
      return state.summary;
      // return { ...state, arraylist:  action.data };
    }

    default:
      return state;
  }
};

