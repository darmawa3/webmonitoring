export interface SummaryState {
    p: number,
    area : string,
    waktu : string,
    suhu : number
}
const SummaryStateKeysEnum: { [K in keyof Required<SummaryState>]: K } = {
    p: 'p',
    area: 'area',
    waktu :'waktu',
    suhu :'suhu'
  };
  
const SummaryStateKeys = Object.values(SummaryStateKeysEnum);


export function isSummary(obj: any): obj is SummaryState {
    let arrofkeys = SummaryStateKeys;
    let num = 0;
    let bool = false;
    for (let i = 0; i <= arrofkeys.length; i++) {
      if (arrofkeys[i] in obj[0]) {
        num += 1;
      }
    }
    if (num == arrofkeys.length) {
      bool = true;
    }
    return bool;
  }


export interface SummaryModel {
    summary : SummaryState[],
}

export type SummaryA = {
    type: "GET_SUMMARY";
    data: SummaryState[]
};


export type SummaryAction = SummaryA;