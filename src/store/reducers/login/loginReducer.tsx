import { LoginAction, LoginModel } from './types';

  const initialState : LoginModel = {
      LoginState: [],
  };
  

  export const loginReducer = (
    state: LoginModel = initialState,
    action: LoginAction
  ) => {
    switch (action.type) {
      case "POST_LOGIN": {
        return { ...state, LoginState:  action.data };
      }
     
      default:
        return state;
    }
  };

