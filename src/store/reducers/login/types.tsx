export interface LoginState {
    message:string,
}
const LoginStateKeysEnum: { [K in keyof Required<LoginState>]: K } = {
    message :'message',
  };
  
const LoginStateKeys = Object.values(LoginStateKeysEnum);


export function isLogin(obj: any): obj is LoginState {
    let arrofkeys = LoginStateKeys;
    let num = 0;
    let bool = false;
    for (let i = 0; i <= arrofkeys.length; i++) {
      if (arrofkeys[i] in obj) {
        num += 1;
      }
    }
    if (num == arrofkeys.length) {
      bool = true;
    }
    return bool;
  }


export interface LoginModel {
    LoginState: LoginState[],
}

export type LoginA = {
    type: "POST_LOGIN";
    data: LoginState[]
};


export type LoginAction = LoginA;