import { Route, Routes } from "react-router-dom"
import Home from "./login"
import Main from "./main"
import Overview from "./overview"
import Perhari from "./perhari"
import Perjam from './perjam'

export function App() {
    return (
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/main" element={<Main/>} />
                    <Route path="/overview" element={<Overview/>} />
                    <Route path="/perjam" element={<Perjam/>} />
                    <Route path="/perhari" element={<Perhari/>} />
                </Routes>
        
    )
}