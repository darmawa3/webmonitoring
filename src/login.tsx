import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import './App.css';
import Cookies from 'universal-cookie';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { SummaryModel, isSummary } from "./store/reducers/summary/types";
import { LoginModel, isLogin } from "./store/reducers/login/types";
import { fetchSummary, fetchLogin } from "../src/store/apicommand/index";
import { fetchSummaryaxios, fetchLoginaxios } from "../src/store/apicommand/axios";
const Login = () => {
  const handleLogin = () => {
    fetchLoginaxios(uname, pw)
      .then(data => {
        if (isLogin(data.data) == true) {
          dispatch(fetchLogin(data.data));
          if (data.data.message == "login berhasil") {
            alert("login berhasil");
            const cookies = new Cookies();
            cookies.set('token', data.data.token, { path: '/main' });
            cookies.set('token', data.data.token, { path: '/overview' });
            cookies.set('token', data.data.token, { path: '/perjam' });
            cookies.set('token', data.data.token, { path: '/perhari' });
            window.location.href = "/main";
          }
          else {
            alert("login gagal!");
          }
        }
        else {
          console.log('none');
        }
      }
      )

  }
  const [uname, setUname] = useState("");
  const [pw, setPw] = useState("");
  const dispatch = useDispatch()



  useEffect(() => {

  }, [])

  return (
    <>
      <Container className="maincontainerlogin">
        <Form className="formcontainer">
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" placeholder="Enter username" defaultValue="" onChange={(e) => setUname(e.target.value)} />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" defaultValue="" onChange={(e) => setPw(e.target.value)} />
          </Form.Group>
        </Form>
        <Button variant="primary" onClick={() => handleLogin()}>
          Login
        </Button>
      </Container>
    </>
  );


}
export default Login;


