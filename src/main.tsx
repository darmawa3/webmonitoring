import { useEffect } from "react";
import './Main.css';
import { useDispatch, useSelector } from 'react-redux'
import { SummaryModel, isSummary } from "./store/reducers/summary/types";
import { fetchSummaryaxios } from "./store/apicommand/axios";
import { fetchSummary } from "./store/apicommand";
import SidebarC from "./components/sidebar";
import NavbarC from "./components/navbar";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Cookies from 'universal-cookie';

const Main = () => {

    useEffect(() => {
        document.getElementsByTagName("body")[0].setAttribute("style", "background-image:none");
        var cookie = new Cookies();
        console.log(cookie.get('token'));
        if(typeof cookie.get('token') == "undefined"){
            document.getElementsByClassName("flexcontainerleft")[0].setAttribute("style", "display:none");
            setTimeout(verifyalert, 500);
        
        }
    }, [])

    const verifyalert = () => {
        window.location.href = "/";
        alert("please verify your credentials first");
    }

    return (
        <>
            <div className="flexcontainerleft">
                <SidebarC />
                <div className="flexcontainerright" >
                    <NavbarC />
                    <div className="headertitlemain">
                        <h1 className="headers">Beranda</h1>
                        <Card className="text-center">
                            <Card.Header>Overview</Card.Header>
                            <Card.Body>
                                <Card.Text>
                                Gambaran sistem monitoring yang meliputi Tegangan masuk, Arus masuk, Daya masuk, Tegangan keluar, Arus keluar, Daya keluar, dan Suhu secara realtime.
                                </Card.Text>
                                <Button variant="primary">Go to overview</Button>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <Card.Header>{'Time Monitoring (Per-Jam)'}</Card.Header>
                            <Card.Body>
                                <Card.Text>
                                    Menampilkan grafik daya listrik dalam 1 jam terakhir.
                                </Card.Text>
                                <Button variant="danger">Go to monitoring</Button>
                            </Card.Body>
                        </Card>
                        <Card className="text-center">
                            <Card.Header>{'Time Monitoring (Harian)'}</Card.Header>
                            <Card.Body>
                                <Card.Text>
                                  Menampilkan grafik daya listrik dalam 1 hari terakhir dibagi per-jam.
                                </Card.Text>
                                <Button variant="success">Go to monitoring</Button>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>
        </>
    );


}
export default Main;

