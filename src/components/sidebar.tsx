import ElectricMeterIcon from "@mui/icons-material/ElectricMeterRounded";
import WatchIcon from "@mui/icons-material/Watch";
import GasMeterOutlinedIcon from "@mui/icons-material/GasMeterOutlined";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";
import { Menu, menuClasses, MenuItem, Sidebar, sidebarClasses, SubMenu, useProSidebar } from "react-pro-sidebar";
import { useEffect, useState } from "react";


const SidebarC = (param1:any) => {
    const { collapseSidebar } = useProSidebar();
    const [boolactive,setBoolactive] = useState(true);
    const [boolactive2,setBoolactive2] = useState(false);
    const [boolactive3,setBoolactive3] = useState(false);
    const [boolactive4,setBoolactive4] = useState(false);
    const [dopen1,setDopen1] = useState(true);
    useEffect(() => {
        document.getElementsByTagName("body")[0].setAttribute("style", "background-image:none");
        if(param1.param1 == "overview"){
           setBoolactive(false);
           setBoolactive2(true);
           setBoolactive3(false);
           setBoolactive4(false);
        }
        else if(param1.param1 == "perjam"){
           setBoolactive(false);
           setBoolactive2(false);
           setBoolactive3(true);
           setBoolactive4(false);
         }
         else if(param1.param1 == "perhari"){
            setBoolactive(false);
            setBoolactive2(false);
            setBoolactive3(false);
            setBoolactive4(true);
          }
    }, [])

    return (
        <>
            <Sidebar style={{ height: "100vh" }}
                rootStyles={{
                    [`.${sidebarClasses.container}`]: {
                        backgroundColor: '#201F6D',
                        color: 'white',

                    },

                }}>
                <Menu

                    menuItemStyles={{
                        button: ({ level, active, disabled }) => {
                            // only apply styles on first level elements of the tree
                            if (active === true)
                                return {
                                    //   color: disabled ? '#f5d9ff' : '#d359ff',
                                    backgroundColor: 'black',
                                    color: "white !important",
                                    borderRadius: "8px !important",
                                    fontWeight: "bold !important",
                                    "&:hover": {
                                        backgroundColor: "#335B8C !important",
                                        color: "white !important",
                                        borderRadius: "8px !important",
                                        fontWeight: "bold !important"
                                    },
                                };
                            else{
                                return {
                                    //   color: disabled ? '#f5d9ff' : '#d359ff',
                                    backgroundColor: '#201F6D',
                                    "&:hover": {
                                        backgroundColor: "#335B8C !important",
                                        color: "white !important",
                                        borderRadius: "8px !important",
                                        fontWeight: "bold !important"
                                    },
                                };
                            }

                            

                        },
                    }}
                >
                    <MenuItem
                        icon={<MenuOutlinedIcon />}
                        onClick={() => {
                            collapseSidebar();
                        }}
                        style={{ marginTop: '20px', marginBottom: '20px' }}

                    >
                        {""}
                        <div style={{ fontWeight: '500', fontSize: '24px' }}>Monitoring</div>
                    </MenuItem>
                    <div style={{ paddingLeft: '25px', marginBottom: '20px' }}>Core</div>
                    <MenuItem id="itemdashboard" active = {boolactive} icon={<GasMeterOutlinedIcon />} onClick={()=>window.location.href="/main"}>Dashboard</MenuItem>
                    <MenuItem id="itemoverview" active = {boolactive2} icon={<ElectricMeterIcon />} onClick={()=>window.location.href="/overview"}>Overview</MenuItem>
                    <SubMenu defaultOpen = {dopen1} rootStyles={{
                        [`.${menuClasses.subMenuContent}`]: {
                            backgroundColor: '#201F6D',
                            color: 'white',


                        },
                        [`.${menuClasses.button}`]: {
                            "&:hover": {
                                backgroundColor: "#335B8C !important",
                                color: "white !important",
                                borderRadius: "8px !important",
                                fontWeight: "bold !important"
                            },


                        },
                    }} label="Time Monitoring" icon={<WatchIcon />}>
                        <MenuItem active = {boolactive3} onClick={()=>window.location.href="/perjam"}>Per-Jam</MenuItem>
                        <MenuItem active = {boolactive4} onClick={()=>window.location.href="/perhari"}>Harian</MenuItem>
                    </SubMenu>
                </Menu>
                <div className="sidebarbottom"><p>{'Logged in as : ' + '‎ ‎ ‎ ' + 'Admin'}</p></div>
            </Sidebar>

        </>
    );


}
export default SidebarC;



