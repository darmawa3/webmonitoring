import { useEffect, useState } from "react";
import { Navbar, Nav, NavItem, NavDropdown } from "react-bootstrap";
import Cookies from "universal-cookie";

const NavbarC = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    const [time,setTime] = useState("");
    const [ticking, setTicking] = useState(true);
    useEffect(() => {
        document.getElementsByTagName("body")[0].setAttribute("style", "background-image:none");
        const timer = setTimeout(() => ticking && realtime(), 1e3)
        return () => clearTimeout(timer)
    }, [time,ticking])

    let today2 = mm + '/' + dd + '/' + yyyy;
    const realtime = () =>{
        let d = new Date();
        let n = d.toLocaleTimeString();
        setTime(n);
    }
    const logout = () => {
        var cookie = new Cookies();
        cookie.remove('token', { path: '/main' });
        cookie.remove('token', { path: '/overview' });
        window.location.href = "/";
    }
    return (
        <>
            <Navbar expand="sm" style={{ width: '100.1%', backgroundColor: 'white', border: 'solid 1px rgb(215,215,215)' }}>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="#home">{today2 + ' ' + time}</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" style={{ position: 'fixed', right: '0', marginRight: '10px' }}>
                    <Nav className="me-auto">
                        <Nav.Link href="#home" onClick={()=>{logout()}}>Logout</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    );


}
export default NavbarC;