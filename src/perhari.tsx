import { useEffect } from "react";
import './perjam.css';
import { useDispatch, useSelector } from 'react-redux';
import { SummaryModel, isSummary } from "./store/reducers/summary/types";
import { fetchSummaryaxios } from "./store/apicommand/axios";
import { fetchSummary } from "./store/apicommand";
import SidebarC from "./components/sidebar";
import NavbarC from "./components/navbar";
import Card from 'react-bootstrap/Card';
import Cookies from 'universal-cookie';
import { Summarize } from "@mui/icons-material";
import { BarChart } from "@mui/icons-material";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { faker } from '@faker-js/faker';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

  export const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
        text: 'Chart.js Line Chart',
      },
    },
  };
  
  const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  
  export const data = {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: labels.map(() => faker.datatype.number({ min: -1000, max: 1000 })),
        borderColor: 'rgb(53, 162, 235)',
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
  };


const Perhari = () => {

    useEffect(() => {
        document.getElementsByTagName("body")[0].setAttribute("style", "background-image:none");
        var cookie = new Cookies();
        console.log(cookie.get('token'));
        if (typeof cookie.get('token') == "undefined") {
            document.getElementsByClassName("flexcontainerleft")[0].setAttribute("style", "display:none");
            setTimeout(verifyalert, 500);

        }
        fetchSummaryaxios()
            .then(data => {
                if (isSummary(data.data) == true) {
                    dispatch(fetchSummary(data.data));
                }
                else {
                    console.log('none');
                }
            }
            )
    }, [])
    const dispatch = useDispatch()

    const store = useSelector<SummaryModel, SummaryModel>(
        (state) => state
    );


    const verifyalert = () => {
        window.location.href = "/";
        alert("please verify your credentials first");
    }

    return (
        <>
            <div className="flexcontainerleft">
                <SidebarC param1={'perhari'} />
                <div className="flexcontainerright" >
                    <NavbarC />
                    <h1 className="perjamheadertitle" onClick={() => console.log(store.summary)}>Data perhari</h1>
                    <div className="flexpanel">
                    <div className="flexcontainerleftpanel">
                        <Card>
                            <Card.Header style={{ display: 'flex' }}><div className="labelicon"><Summarize /></div> Total konsumsi energi listrik</Card.Header>
                            <Card.Body>
                                {typeof store.summary[0] !== "undefined" ? (
                                    <>
                                    <div className="flexlabel"><p className="t1">Area</p><p className="t2">:  {store.summary[store.summary.length -2].area}</p></div>
                                    <div className="flexlabel"><p className="t1">Waktu</p><p className="t2">:  {store.summary[store.summary.length -2].waktu}</p></div>
                                    <div className="flexlabel"><p className="t1">Data terakhir</p><p className="t2">:  {store.summary[store.summary.length -2].waktu.slice(10)}</p></div>
                                    <div className="flexlabel"><p className="t1">Hari ini</p><p className="t2">:  0.00 kWh</p></div>
                                    <div className="flexlabel"><p className="t1">Rata rata hari ini</p><p className="t2">:  0.00 kW</p></div>
                                    <div className="flexlabel"><p className="t1">{'Biaya(total)'}</p><p className="t2">:  Rp. 115 ,00</p></div>
                                    </>
                                ) : (
                                    <div>...loading</div>
                                )}

                            </Card.Body>
                        </Card>
                    </div>
                    <div className="flexcontainerrightpanel">
                    <Card>
                            <Card.Header style={{ display: 'flex' }}><div className="labelicon"><BarChart /></div> Chart realtime overview</Card.Header>
                            <Card.Body>
                             
                            <Bar options={options} data={data} />
                            </Card.Body>
                        </Card>
                        </div>
                        </div>
                </div>
            </div>
        </>
    );


}
export default Perhari;

