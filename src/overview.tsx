import { useEffect, useState } from "react";
import './overview.css';
import { useDispatch, useSelector } from 'react-redux'
import { OverviewModel, isOverview } from "./store/reducers/overview/types";
import { fetchOverviewaxios } from "./store/apicommand/axios";
import { fetchOverview } from "./store/apicommand";
import SidebarC from "./components/sidebar";
import NavbarC from "./components/navbar";
import GaugeChart from 'react-gauge-chart'
import Cookies from 'universal-cookie';

const Overview = () => {

    const dispatch = useDispatch()
    const store = useSelector<OverviewModel, OverviewModel>(
        (state) => state
    );

    const chartStyle = {
        width: '250px',
        height: '5px',
        // marginBottom: '-100px',
        paddingLeft: '60px',
        marginRight: '120px',
        // marginTop:'-120px'
    }

    const fetchMyAPI = () => {
        fetchOverviewaxios()
        .then(data => {
            if (isOverview(data.data) == true) {
                dispatch(fetchOverview(data.data));
            }
            else {
                console.log('none');
            }
        }
        )
    }
    const fetchInterval = () => {
        const interval = setInterval(() => {
            fetchMyAPI();
            // console.log("test")
          }, 2000);
          return () => clearInterval(interval);
    } 

    useEffect(() => {
       
        document.getElementsByTagName("body")[0].setAttribute("style", "background-image:none;");

        var cookie = new Cookies();
        console.log(cookie.get('token'));
        if (typeof cookie.get('token') == "undefined") {
            document.getElementsByClassName("flexcontainerleft")[0].setAttribute("style", "display:none");
            setTimeout(verifyalert, 500);

        }
        else{
            fetchInterval();
        }
    }, [])

    const verifyalert = () => {
        window.location.href = "/";
        alert("please verify your credentials first");
    }

    return (
        <>
            <div className="flexcontainerleft">
                <SidebarC param1={"overview"} />
                <div className="flexcontainerright">
                    <NavbarC />
               
                    {typeof store.overview[0] !== "undefined" ? (
                          <div className="headertitleoverview">
                          {/* <div>GaugeChart with default props</div> */}
                          <div className="h1">Overview</div>
                          <div className="headertitleoverview2">
                          <div className="gridgauge">
                              <div className="labelgauge">{'Tegangan masuk (V_IN)'}</div>
                              <GaugeChart id="gauge-chart1" style={chartStyle} nrOfLevels={420}
                                  arcsLength={[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]}
                                  colors={['#201F6D', 'green']}
                                  percent={store.overview[0].v_in / 100 ?? 0}
                                  arcPadding={0.02}
                                  formatTextValue={value => value + ' V'}
                              />
                          </div>
                          <div className="gridgauge">
                              <div className="labelgauge">{'Arus masuk (I_IN)'}</div>
                              <GaugeChart id="gauge-chart1" style={chartStyle} nrOfLevels={420}
                                  arcsLength={[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]}
                                  colors={['#201F6D', 'green']}
                                  percent={store.overview[0].i_in / 100 ?? 0}
                                  arcPadding={0.02}
                                  formatTextValue={value => value + ' V'}
                              />
                          </div>
                          <div className="gridgauge">
                              <div className="labelgauge">{'Daya masuk (P_IN)'}</div>
                              <GaugeChart id="gauge-chart1" style={chartStyle} nrOfLevels={420}
                                  arcsLength={[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]}
                                  colors={['#201F6D', 'green']}
                                  percent={store.overview[0].p_in / 100 ?? 0}
                                  arcPadding={0.02}
                                  formatTextValue={value => value + ' A'}
                              />
                          </div>
                          <div className="gridgauge">
                              <div className="labelgauge">{'Tegangan keluar (V_OUT)'}</div>
                              <GaugeChart id="gauge-chart1" style={chartStyle} nrOfLevels={420}
                                  arcsLength={[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]}
                                  colors={['#201F6D', 'green']}
                                  percent={store.overview[0].v_out / 100 ?? 0}
                                  arcPadding={0.02}
                                  formatTextValue={value => value + ' A'}
                              />
                          </div>
                          <div className="gridgauge">
                              <div className="labelgauge">{'Arus keluar (I_OUT)'}</div>
                              <GaugeChart id="gauge-chart1" style={chartStyle} nrOfLevels={420}
                                  arcsLength={[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]}
                                  colors={['#201F6D', 'green']}
                                  percent={store.overview[0].i_out / 100 ?? 0}
                                  arcPadding={0.02}
                                  formatTextValue={value => value + ' W'}
                              />
                          </div>
                          <div className="gridgauge">
                              <div className="labelgauge">{'Daya keluar (P_OUT)'}</div>
                              <GaugeChart id="gauge-chart1" style={chartStyle} nrOfLevels={420}
                                  arcsLength={[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]}
                                  colors={['#201F6D', 'green']}
                                  percent={store.overview[0].p_out / 100 ?? 0}
                                  arcPadding={0.02}
                                  formatTextValue={value => value + ' W'}
                              />
                          </div>
                          </div>
                      </div>
                        
                    ) : (
                        <div className="headertitleoverview"><div className="h1loading">loading...</div></div>
                    )}
                  
                </div>
            </div>
        </>
    );


}
export default Overview;

